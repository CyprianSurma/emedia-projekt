#include "BMPLoader.h"
#include <stdio.h>
#include <algorithm>
#pragma packed
bool BMPLoader::load(const char *name)
{
	FILE *file;
	if (fopen_s(&file, name, "rb"))
	{
		return false;
		}
	if(!fread(&m_header, sizeof(BMPHeader), 1, file))
		return false;
	if (!fread(&m_info, sizeof(BMPInfo), 1, file))
		return false;
	m_info.biSizeImage = m_info.biHeight*m_info.biWidth*3;
	m_data =(unsigned char*) malloc(m_info.biSizeImage);
	fread(m_data,1, m_info.biSizeImage, file);
	
	for (int imageIdx = 0; imageIdx < m_info.biSizeImage; imageIdx += 3)
	{
		std::swap(m_data[imageIdx], m_data[imageIdx + 2]);
	}
	

}

BMPLoader::BMPLoader()
{
}


BMPLoader::~BMPLoader()
{
	delete[] m_data;
}

void BMPLoader::save(const char * name)
{

	FILE *file;
	if (fopen_s(&file, name, "ab+"))
	{
		return;
	}
	fwrite(&m_header, sizeof(BMPHeader), 1, file);
		
	fwrite(&m_info, sizeof(BMPInfo), 1, file);
	
	for (int imageIdx = 0; imageIdx < m_info.biSizeImage; imageIdx += 3)
	{
		std::swap(m_data[imageIdx], m_data[imageIdx + 2]);
	}

	fwrite(m_data, 1, m_info.biSizeImage, file);
	fclose(file);


}
