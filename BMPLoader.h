#pragma once
#include <cstdint>
#include <Windows.h>
#include<stdio.h>

using BMPHeader = BITMAPFILEHEADER;

using BMPInfo = BITMAPINFOHEADER;
class BMPLoader
{
public:
	bool load(const char*);
		unsigned char * m_data=nullptr;
	BMPLoader();
	~BMPLoader();
	uint32_t dataSize() { return m_info.biSizeImage; }
	void save(const char*);
private :
	
		BMPHeader m_header;
		BMPInfo m_info;
	
};

