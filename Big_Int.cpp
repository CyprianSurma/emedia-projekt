#include "Big_Int.h"

std::pair<Big_Int, Big_Int> Big_Int::divide(const Big_Int &val)const
{
	Big_Int result("0");
	Big_Int cp("0");
	for (auto i : _number)
	{
		cp.push_back(i);
		if (!(val>cp))
		{
			result.push_back(true);
			cp = cp - val;
		}
		else
		{
			result.push_back(false);
		}
	}






	return{ result,cp };
}

void Big_Int::simplify()
{
	_number.erase(_number.begin(), _number.begin() + (_number.size() - length()));
}
 Big_Int& Big_Int::operator+=( Big_Int  val)
{
	
	bool reminder = false;
	if (this->size() < val.size())
	{
		this->push_front(0, val._number.size()- this->_number.size());
	}
	if (  val.size()<this->size())
	{
		val.push_front(0,  this->_number.size()-val._number.size() );
	}
	for (uint64_t i=0; i < val.size(); ++i)
	{
		uint64_t j = val.size() - i-1;
		int help = 0;
		help +=static_cast<int>( reminder);
		help += static_cast<int>((*this)._number[j]);
		help += static_cast<int>(val._number[j]);
		reminder = false;
		switch (help)
		{
		case 0 :
		{
			(*this)._number[j] = 0;
			break;
		}
		case 1 :
			(*this)._number[j] = 1;
			break;
		case 2:
			(*this)._number[j] = 0;
			reminder = true;
			break;
		case 3:
			(*this)._number[j] = 1;
			reminder = true;
			break;
		default:
			break;
		}
	}
	if (reminder)
	{
		this->push_front(1,1);
	}
	return *this;
	
}
 Big_Int & Big_Int::operator*=(const Big_Int & val)
 {
	 Big_Int cp("0");
	 for (uint64_t i = 0; i < val.size(); ++i)
	 {
		 if (val._number[i] == 1)
		 {
			 auto a = (*this << val.size() - i-1);
			 cp += (*this << val.size()-i-1);
		 }
	 }
	 *this = cp;
	 return *this;
 }
 Big_Int Big_Int::operator+(const Big_Int & val)const 
 {
	 Big_Int result("0");
	 result += *this;
	 result += val;
	 return result;
 }
 bool Big_Int::operator>(const Big_Int &val)const
 {
	 if (length() >val.length())
	 {
		 return true;
	 }
	 if (length() == val.length())
	 {
		 auto i = val.size() - val.length();
		 auto j = size() - length();
		 for (; i < val.size(); ++i, ++j)
		 {
			 if (_number[j] != val._number[i])
			 {
				 return _number[j];
			 }
		 }
	 }

	 return false;
 }
 Big_Int  Big_Int::operator-(const Big_Int &val)const
 {
	 Big_Int result(*this);
	 assert((!( val>*this))&&"Result cant be negative");
	 for (auto i = 1; i <val.length()+1; ++i)
	 {
		 auto a = val._number[val.size() - i];        //0-0 or 1-0

		 if (val._number[val.size()-i] == false)       //0-0 or 1-0
		 {		 
		 }
		 else
		 {
			 if (result._number[size() - i] == true)  //1-1
			 {
				 result._number[size() - i] = false;
			 }
			 else    //0-1 
			 {
				 result._number[size() - i] = true;

				 auto j = result.size() - i - 1;
				 while (1)
				 {
					 if (result._number[j] == true)
					 {
						 result._number[j] = false;
						 break;
					 }
					 else
					 {
						 result._number[j] = true;
					 }
					 --j;
				 }
				 
			 }
		 }
	}
	 return result;
	 // TODO: insert return statement here
 }
 Big_Int Big_Int::operator/(const Big_Int &val) const
 {
	 return divide(val).first;
 }
 Big_Int Big_Int::operator%(const Big_Int &val) const
 {
	 return divide(val).second;
 }
Big_Int Big_Int::powModulo(const Big_Int &val, const Big_Int &pow, const Big_Int &mod)
 {
	 Big_Int result("1");
	 Big_Int help = val% mod;
	 
	
	
		 for (int j=pow._number.size()-1;j>=0;--j){
			 auto i = pow._number[j];
		 if (i)
		 {
			 result *= help;
			 result = result%mod;
		 }
		 result.simplify();
		 help *= help;
		 help.simplify();
		 help = help%mod;
	 }
	 
	 return result;
 }
unsigned char Big_Int::uchar ()const
{
	unsigned char result=0;
	for (uint64_t i=0;i<_number.size();++i)
	{
		if (_number[_number.size()-1-i])
		{
			result += pow(2, i);
	   }
	}
	return result;
}
Big_Int::Big_Int(const std::string str)
{
	_number.reserve(str.size());
	for (auto&i : str)
	{
		push_back(i - '0');
	}
}
Big_Int::Big_Int(const unsigned char &character)
{
	std::bitset<8> a(character);
	for (uint8_t i = 0; i < 8; ++i)
	{
		push_front(a[i], 1);
	}
	simplify();
}
Big_Int::Big_Int()
{
}

Big_Int::~Big_Int()
{
}

Big_Int Big_Int::operator*(const Big_Int & val)const 
{
	Big_Int cp("1");
	cp *= val;
	cp *= *this;
	return cp;
	return Big_Int();
}

uint64_t Big_Int::length()const
{
	
	uint64_t result=_number.size();
	{
		for (auto i : _number)
		{
			if (i == true)
			{
				break;
			}
			else
			{
				result--;
			}
		}
	}
	if (result == 0 && _number.size() != 0)
	{
		result = 1;
	}
	return result;
}