#pragma once
#include <vector>
#include <string>
#include <assert.h>
#include <bitset>
class Big_Int
{
public:
	std::pair<Big_Int, Big_Int> divide(const Big_Int&)const;
	void simplify();
	uint64_t length()const;
	Big_Int(const std::string);
	Big_Int(const unsigned char&);
	void push_front(bool v, uint64_t len) { std::vector<bool> vec(len, v); _number.insert(_number.begin(), vec.begin(), vec.end()); }
	void push_back(bool v) { _number.push_back(v); }
	Big_Int();
	~Big_Int();
	inline Big_Int & operator =(const Big_Int & val) { this->_number = val._number; return *this; }
	Big_Int operator *(const Big_Int & val)const;
	inline Big_Int operator <<(const uint64_t & val) { Big_Int result(*this);for (uint64_t i = 0; i < val; ++i) { result.push_back(0); }return result; }
	inline Big_Int operator ++() { return *this += {"1"}; }
	Big_Int& operator +=(Big_Int val);
	Big_Int& operator *=(const Big_Int &val);
	Big_Int operator +(const Big_Int &val)const;
	inline uint64_t size()const { return _number.size(); }
	bool operator >(const Big_Int&)const;
	Big_Int operator - (const Big_Int&)const;
	Big_Int operator / (const Big_Int&) const;
	Big_Int operator % (const Big_Int&)const;
	static Big_Int powModulo(const Big_Int&, const Big_Int&, const Big_Int&);
	unsigned char uchar()const;
private:
	std::vector<bool> _number;
	


};

