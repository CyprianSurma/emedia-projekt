#include "DFT.h"

#include <valarray>
#include<iostream>
const double PI = 3.141592653589793238460;

void fftshift2D(std::vector<std::complex<float>> data, size_t xdim, size_t ydim)
{
	size_t xshift = xdim / 2;
	size_t yshift = ydim / 2;
	if ((xdim*ydim) % 2 != 0) {
		// temp output array
		std::vector<std::complex<float> > out;
		out.resize(xdim * ydim);
		for (size_t x = 0; x < xdim; x++) {
			size_t outX = (x + xshift) % xdim;
			for (size_t y = 0; y < ydim; y++) {
				size_t outY = (y + yshift) % ydim;
				// row-major order
				out[outX + xdim * outY] = data[x + xdim * y];
			}
		}
		// copy out back to data
		copy(out.begin(), out.end(), &data[0]);
	}
	else {
		// in and output array are the same,
		// values are exchanged using swap
		for (size_t x = 0; x < xdim; x++) {
			size_t outX = (x + xshift) % xdim;
			for (size_t y = 0; y < yshift; y++) {
				size_t outY = (y + yshift) % ydim;
				// row-major order
				swap(data[outX + xdim * outY], data[x + xdim * y]);
			}
		}
	}
}


// Cooley�Tukey FFT (in-place)
typedef std::complex<double> complexDobule; 
typedef std::vector<complexDobule> complexDoubleVector;

complexDoubleVector fft(const complexDoubleVector &as) {
	int n = as.size();

	if (n == 1) return complexDoubleVector(1, as[0]);

	complexDoubleVector w(n);
	for (int i = 0; i < n; i++) {
		double alpha = 2 * PI * i / n;
		w[i] = complexDobule(cos(alpha), sin(alpha));
	}


	complexDoubleVector A(n / 2), B(n / 2);
	for (int i = 0; i < n / 2; i++) {
		A[i] = as[i * 2];
		B[i] = as[i * 2 + 1];
	}
	complexDoubleVector Av = fft(A);
	complexDoubleVector Bv = fft(B);
	complexDoubleVector res(n);
	for (int i = 0; i < n; i++)
		res[i] = Av[i % (n / 2)] +
		w[i] * Bv[i % (n / 2)];
	return res;
}
std::vector<unsigned char> DFT::amplitudeSpectrum(unsigned char *a,const uint32_t sizeX,const uint32_t sizeY)
{
	unsigned char* b=new unsigned char [sizeX*sizeY * 3];
	for (int i = 0; i < sizeX*sizeY * 3; i += 3)
	{
		auto p = (a[i] + a[i + 1] + a[i + 2]) / 3;
		b[i] = p;
		b[i + 1] = p;
		b[i + 2] = p;

	}
	std::vector<float> result1(sizeX*sizeY * 3);
	auto dftresult=calcDFT(b, sizeX, sizeY);
	fftshift2D(dftresult, sizeX, sizeY);
	for (uint32_t i = 0; i < result1.size(); ++i)
	{
		auto k= std::arg(dftresult[i]);
//		std::cout << k<<" ";
		result1[i] = std::arg(dftresult[i]);
	}
	float min = 1000000000000, max = -1000000000000000000;
	for (auto i : result1)
	{
		if (i < min)
		{
			min = i;
		}
		if (i > max)
		{
			max = i;
		}
	}

	std::vector<unsigned char> result;
	for (auto& i : result1)
	{
		result.push_back(i*255/(max));
	}
	return std::move(result);
}

DFT::DFT()
{
}


DFT::~DFT()
{
}

std::vector< std::complex<float>> DFT::calcDFT(unsigned char *image, uint32_t sizeX,uint32_t sizeY)
{
	std::vector< std::complex<float>> res(sizeX*sizeY * 3);
	std::vector< std::complex<float>> res1(sizeX*sizeY * 3);
	complexDoubleVector Ry(sizeY);
	complexDoubleVector Gy(sizeY);
	complexDoubleVector By(sizeY);
	complexDoubleVector Rx(sizeX);
	complexDoubleVector Gx(sizeX);
	complexDoubleVector Bx(sizeX);
	for (uint32_t i = 0; i < sizeX * 3; i += 3) {  // obliczanie fft w pionie
		for (uint32_t j = 0; j <  sizeY* 3; j += 3) {
			Ry[sizeY-1-j / 3] = image[i*sizeY + j];
			Gy[sizeY - 1 - j / 3] = image[i*sizeY + j+1];
			By[sizeY - 1 - j / 3] = image[i*sizeY + j+2];
		
		}
		auto R1=fft(Ry);
		auto G1 = fft(Gy);
		auto B1 = fft(By);
		for (uint32_t j = 0; j < sizeY * 3; j += 3) {
			res[i*sizeY + j] = R1[j / 3];
			res[i*sizeY + j + 1] = G1[j / 3];
			res[i*sizeY + j + 2] = B1[j / 3];

		}
	}
	for (uint32_t j = 0; j < sizeY * 3; j += 3) {
		for (uint32_t i = 0; i < sizeX * 3; i += 3) {
			Rx[sizeY - 1 - j / 3] = res[i*sizeY + j];
			Gx[sizeY - 1 - j / 3] = res[i*sizeY + j + 1];
			Bx[sizeY - 1 - j / 3] = res[i*sizeY + j + 2];

		}
		auto R1 = fft(Rx);
		auto G1 = fft(Gx);
		auto B1 = fft(Bx);
		for (uint32_t i = 0; i < sizeX * 3; i += 3) {
			res1[i*sizeY + j] = R1[j / 3];
			res1[i*sizeY + j + 1] = G1[j / 3];
			res1[i*sizeY + j + 2] = B1[j / 3];

		}
	}

	return std::move(res1);
}
