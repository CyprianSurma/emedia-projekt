#pragma once
#include <cstdint>
#include <complex>
#include <vector>
const auto pi = 3.14159265358979323846;

class DFT
{
public:
	
	std::vector<unsigned char> amplitudeSpectrum(unsigned char*,const uint32_t ,const uint32_t);
	DFT();
	~DFT();
private:
	
	std::vector< std::complex<float>> calcDFT(unsigned char*, uint32_t,uint32_t);
};
