#include "RSA.h"
#include<iostream>
#include <chrono>

bool RSA::init(char *inFileName, char *outFileName)
{
	inFile.open(inFileName, std::ios::in);
	if (!inFile.good())
	{
		std::cout << "zly plik wejsciowy\n";
		return false;
	}
	outFile.open(outFileName, std::ios::out|std::ios::trunc);
	if (!outFile.good())
	{
		std::cout << "zly plik wyjsciowy\n";
		return false;
	}
	return true;
}

RSA::RSA() 
{
}


RSA::~RSA()
{
	outFile.close();
	
	inFile.close();
}

unsigned char RSA::codeChar(const Big_Int& val )const
{
	
	auto res=Big_Int::powModulo(val, publicKey, nVal);
	
	return res.uchar();

}

void RSA::codeThreadFunction()
{
	unsigned char res,inChar;
	uint64_t charIndex;
	while (1)
	{
		{
			std::lock_guard<std::mutex> lock(inFileMutex);
			if (!inFile.good())
			{
				
				break;
			}
			else
			{
				int x;
				inFile >> x;
				inChar = x;
			
				charIndex = index++;
			}

		}
		Big_Int z(inChar);
		auto p = z.uchar();
		res = codeChar({ inChar });




		{
			std::lock_guard<std::mutex> lock(charactersMutex);
			characters.push({ res, charIndex });
		}
	}
}
void RSA::queueThreadFunction()
{
	uint64_t charIndex = 0;
	while (1)
	{
		{
			std::unique_lock<std::mutex> lock(charactersMutex);
			if (codeFinished)                                      //if other threads finished job
			{
				while (!characters.empty())                        // put all characters from q into file
				{
					outFile <<(int) characters.top().first<<' ';
					characters.pop();
				}
				break;
			}
			else
				if (characters.empty())                           //if q is qmpty sleep for 0.2s
				{
					lock.unlock();
					std::this_thread::sleep_for(std::chrono::milliseconds(200));
				}
				else
				{
					if (characters.top().second == charIndex)     // check if correct character is on the top of q
					{
						outFile << (int)characters.top().first << ' ';
						characters.pop();
						++charIndex;
					}
					else                                            // if not sleep for 0.2s
					{
						lock.unlock();
						std::this_thread::sleep_for(std::chrono::milliseconds(200));
					}
				}
		}
	}
}

void RSA::code()
{
	std::thread queueThread(&RSA::queueThreadFunction,this);
	std::vector<std::thread> threads;
	for (uint8_t i = 0; i < threadNum; ++i)
	{
		threads.push_back( std::thread(&RSA::codeThreadFunction,this) );
	}

	for (auto &i:threads)
	{
		i.join();
	}
	codeFinished = true;
	queueThread.join();
}

void RSA::decode()
{
	std::swap(publicKey, privateKey);
	code();
	std::swap(publicKey, privateKey);
}
