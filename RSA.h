#pragma once
#include<thread>
#include <mutex>
#include <fstream>
#include <queue>
#include "Big_Int.h"
#include <vector>
#include <atomic>

class RSA
{
public:
	static const int threadNum = 8;
	void code();
	void decode();
	bool init(char*, char*);
	void setPublicKey(const Big_Int &val) { publicKey = val; };
	void setPrivateKey(const Big_Int &val) { privateKey = val; };
	void setnVal(const Big_Int &val) { nVal = val; };
	RSA();
	~RSA();
private:
	struct comp
	{		
		bool operator()(std::pair<unsigned char, uint64_t>a, std::pair<unsigned char, uint64_t>b){return a.second > b.second; };
	};
	std::atomic_bool codeFinished = false;
	uint64_t index = 0;
	std::fstream inFile
				,outFile;
	std::mutex inFileMutex;   //index also locked with this mutex
	Big_Int publicKey
		, privateKey
		, nVal;
	std::priority_queue < std::pair<unsigned char, uint64_t>,std::vector<std::pair<unsigned char, uint64_t>>,comp >characters;
	std::mutex charactersMutex;

private:
	
	unsigned char codeChar(const Big_Int& val)const;
	void codeThreadFunction();
	void queueThreadFunction();
};

